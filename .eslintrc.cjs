/* eslint-env node */
module.exports = {
    parser: '@typescript-eslint/parser',
    extends: [
      'eslint:recommended',
      'plugin:@typescript-eslint/strict',
      'plugin:@typescript-eslint/strict-type-checked',
      'plugin:@typescript-eslint/stylistic-type-checked',
      'prettier'
    ],
    plugins: ['@typescript-eslint', 'prettier'],
    root: true,
    parserOptions: {
      project: true,
      tsconfigRootDir: __dirname,
    },
    rules: {
      "prettier/prettier": "error",
      "@typescript-eslint/no-misused-promises": [
        "error",
        {
          "checksVoidReturn": false
        }
      ]
    },
  };
  